
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import Toaster from 'v-toaster'
import { store } from './store/store.js'
import 'v-toaster/dist/v-toaster.css'

Vue.use(BootstrapVue)
Vue.use(Toaster)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
