import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    teamlist: []
  },
  mutations: {
    UpdateRole (state, payload) {
      state.teamlist = payload.teamlist
    }
  }
})
export default store
