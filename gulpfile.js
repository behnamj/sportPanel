'use strict'

var gulp = require('gulp');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');



gulp.task('fonts', function() {
    return gulp.src('./Fonts/**/*.woff')
        .pipe(gulp.dest('./static/fonts'));
});


gulp.task('watch', function() {
    gulp.watch([
      ('/Fonts/**/*')
    ])
    gulp.watch(['./Fonts/**/*.woff'], ['fonts']);
});


gulp.task('build', function(callback) {
    runSequence('fonts');
});


gulp.task('default', function() {
    gulp.start('build');
});

